
DESCRIPTION
-----------
This module allows access to nodes with attached and related files, as
well as access to file node contents, via a WebDAV metaphor similar to
FileServerModule. The user can browse all files existing in the system
by file MIME type, attached- or related-to node type (e.g., event,
story, etc.), and attached- or related-to node parent group. Group
browsing additionally supports OgVocabModule integration, which browses
by group taxonomy to find attached- or related-to nodes.

The module requires DavModule, FileFramework, FileAttachModule, and
OgModule. If OgVocabModule is enabled, an option is provided to allow
unattached/unrelated group file nodes to appear by group taxonomy, and
if RelationsModule is enabled, options exists for each node type to
determine whether that type uses file attachments or RDF relationships
to relate files in the WebDAV metaphor.

Full support is provided for uploading new files (thereby creating file
nodes) and managing tagging, attachment status, RDF relationship status,
and filename. Files can be uploaded to create file nodes and make new
associations, moved to change associations, copied to create new
associations, and deleted to remove associations. If so configured for
the file node type, full node revision support is enabled, leaving a
paper trail of all changes made through the WebDAV interface.

INSTALLATION
------------
Please refer to the accompanying file INSTALL.txt for installation
requirements and instructions.

CREDITS
-------
Developed by Justin R. Miller <http://codesorcery.net/>
Based on File Server by Arto Bendiken <http://bendiken.net/>
Sponsored by MakaluMedia Group <http://www.makalumedia.com/>
Sponsored by M.C. Dean, Inc. <http://www.mcdean.com/>
Sponsored by SPAWAR <http://www.spawar.navy.mil/>
