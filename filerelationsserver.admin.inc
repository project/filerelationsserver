<?php

/**
 * @file
 * Provides settings page.
 */

/**
 * Admin settings form, called by hook_menu().
 */
function filerelationsserver_admin_settings_form() {

  // This is required for proper checkbox handling per system.module.
  $form['array_filter'] = array('#type' => 'hidden');

  // Get all of the node and group node types.
  $node_types = node_get_types();
  $group_node_types = module_exists('og') ? og_get_types('group') : array();

  // Component settings, defaulting to all by default.
  $form['components'] = array(
    '#title'       => t('Components'),
    '#type'        => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );
  $form['components']['filerelationsserver_components'] = array(
    '#title'         => t('Enabled browsing methods'),
    '#type'          => 'checkboxes',
    '#options'       => array(
      FILERELATIONSSERVER_NODE_TYPE => t('Browse by node type'),
      FILERELATIONSSERVER_FILE_TYPE => t('Browse by file type'),
    ),
    '#default_value' => _filerelationsserver_variable_get('filerelationsserver_components'),
    '#description'   => t('Choose which components to enable in the WebDAV listing.'),
  );
  if (module_exists('og'))
    $form['components']['filerelationsserver_components']['#options'][FILERELATIONSSERVER_GROUP] = t('Browse by group');

  // RDF relations integration.
  if (module_exists('relations')) {
    $form['relations'] = array(
      '#title'       => t('Relation methods'),
      '#type'        => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );
    $form['relations']['relations_header'] = array(
      '#prefix'      => '<div>',
      '#value'       => t('Each type of node in the system can be configured to show and maintain associations using either file attachments (the default) or, since !relations is enabled, RDF relationships.', array('!relations' => l('the Relations API', 'admin/settings/rdf/relations'))),
      '#suffix'      => '</div>',
    );
    foreach ($node_types as $type => $info) {
      if (!in_array($type, $group_node_types)) {
        $form['relations']['filerelationsserver_relation_method_'. $type] = array(
          '#title'         => $info->name . t(' nodes'),
          '#type'          => 'select',
          '#default_value' => variable_get('filerelationsserver_relation_method_'. $type, FILERELATIONSSERVER_RELATION_ATTACHMENT),
          '#options'       => array(
            FILERELATIONSSERVER_RELATION_ATTACHMENT => t('Use file attachments'),
            FILERELATIONSSERVER_RELATION_RDF        => t('Use RDF relationships'),
          ),
        );
        $form['relations']['filerelationsserver_show_empty_'. $type] = array(
          '#type'          => 'checkbox',
          '#title'         => t('Exclude nodes without file relations.'),
          '#default_value' => variable_get('filerelationsserver_show_empty_'. $type, 0),
        );
      }
    }
  }

  // Og_vocab integration.
  if (module_exists('og_vocab')) {
    $form['og_vocab'] = array(
      '#title'       => t('Organic groups integration'),
      '#type'        => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );
    $form['og_vocab']['filerelationsserver_og_vocab'] = array(
      '#title'         => t('Show nodes by organic group taxonomy'),
      '#type'          => 'radios',
      '#options'       => array(FALSE => t('Disabled'), TRUE => t('Enabled')),
      '#default_value' => FILERELATIONSSERVER_OG_VOCAB,
      '#description'   => t('Enabling this will show nodes by group taxonomy when browsing by group. Otherwise, all nodes for a given group will be listed in the same DAV folder.'),
    );
  }

  // Exclusion settings.
  $form['exclusions'] = array(
    '#title'       => t('Exclusions'),
    '#type'        => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );

  // Node type exclusions, defaulting to file & group node type(s).
  $form['exclusions']['excluded_node_types'] = array(
    '#title'       => t('Nodes'),
    '#type'        => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  foreach ($node_types as $type => $info) {
    if (!in_array($type, $group_node_types)) {
      $node_options[$type] = $info->name;
    }
  }
  asort($node_options);
  $form['exclusions']['excluded_node_types']['filerelationsserver_excluded_node_types'] = array(
    '#title'         => t('Excluded node types'),
    '#type'          => 'checkboxes',
    '#options'       => $node_options,
    '#default_value' => _filerelationsserver_variable_get('filerelationsserver_excluded_node_types'),
    '#description'   => t('Choose which node types to exclude from the WebDAV listing when browsing by node type.'),
  );

  if (module_exists('og')) {
    // Group exclusions, defaulting to none.
    $form['exclusions']['excluded_groups'] = array(
      '#title'       => t('Groups'),
      '#type'        => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    );
    $group_result = db_query("SELECT nid, title FROM {node} WHERE type IN (". db_placeholders($group_node_types, 'varchar') .")", $group_node_types);
    while ($group = db_fetch_object($group_result)) {
      $group_options[$group->nid] = $group->title;
    }
    asort($group_options);
    $form['exclusions']['excluded_groups']['filerelationsserver_excluded_groups'] = array(
      '#title'         => t('Excluded groups'),
      '#type'          => 'select',
      '#options'       => $group_options,
      '#default_value' => _filerelationsserver_variable_get('filerelationsserver_excluded_groups'),
      '#multiple'      => TRUE,
      '#description'   => t('Choose which groups to exclude from the WebDAV listing when browsing by group.'),
    );
  }

  // File type exclusions, defaulting to none.
  $form['exclusions']['excluded_file_types'] = array(
    '#title'       => t('Files'),
    '#type'        => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  $file_types = file_get_mime_types();
  foreach ($file_types as $type => $info) {
    $file_type_options[$info['name']] = $info['name'];
  }
  asort($file_type_options);
  $form['exclusions']['excluded_file_types']['filerelationsserver_excluded_file_types'] = array(
    '#title'         => t('Excluded file types'),
    '#type'          => 'checkboxes',
    '#options'       => $file_type_options,
    '#default_value' => _filerelationsserver_variable_get('filerelationsserver_excluded_file_types'),
    '#description'   => t('Choose which file types to exclude from the WebDAV listing when browsing by group.'),
  );

  // Miscellaneous.
  $form['misc'] = array(
    '#title'       => t('Miscellaneous'),
    '#type'        => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
  );
  $form['misc']['filerelationsserver_debug'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Debug'),
    '#description'   => t('The debug output will be written to the webserver\'s error log.'),
    '#default_value' => FILERELATIONSSERVER_DEBUG,
  );

  return system_settings_form($form);
}

